FROM registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest

RUN apt update -y
RUN curl --silent --location https://deb.nodesource.com/setup_14.x | bash -

RUN apt update && \
  apt -y install \
  git \
  jq \
  nodejs \
  unzip \
  python3-dev \
  gcc \
  zlib1g \
  zlib1g-dev \
  wget

RUN wget https://github.com/aws/aws-sam-cli/releases/latest/download/aws-sam-cli-linux-x86_64.zip && \
  unzip aws-sam-cli-linux-x86_64.zip -d sam-installation && \
  ./sam-installation/install

RUN sam --version

RUN curl -O https://bootstrap.pypa.io/get-pip.py && \
    python3 get-pip.py
RUN python3 -m pip install --upgrade \
  aws-whoami \
  cfn-lint \
  pip \
  yq

# update npm to latest version
RUN npm install -g npm
RUN npm install -g yarn
RUN npm install -g ts-node
